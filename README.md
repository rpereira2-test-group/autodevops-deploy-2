# GDK friendly deploy script

This repository contains a .gitlab-ci.yml file that has been modified to perform only the deploy stage. Further, it hardcodes a sample ruby image, as the GDK does not contain the registry.

## Deploy multiple pods

Set a [variable](https://gitlab.com/help/ci/variables/README#variables) `REPLICAS` to the number of pods that are needed.

## Deploy multiple containers in a pod

1. Download all files from the https://gitlab.com/gitlab-org/charts/auto-deploy-app repo.
2. Add a new folder called `chart` in your `autodevops-deploy` repo, and place
all the files from the https://gitlab.com/gitlab-org/charts/auto-deploy-app repo
in the chart folder.
3. Modify the `chart/templates/deployment.yaml` file in your `autodevops-deploy`
repo. By default, there is only one container defined under the spec -> template -> spec -> containers
key. Add as many containers as needed. To do this, add a new entry under containers
by copying all the keys of the existing container. Make sure to give each
container a unique name. [Here is an example deployment.yaml](https://gitlab.com/snippets/1904912)
file with [2 containers defined](https://gitlab.com/snippets/1904912#L35).